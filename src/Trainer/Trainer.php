<?php

namespace App\Trainer;

if(!isset($_SESSION)){
    session_start();
}

use App\Connection;
use PDO;
use PDOException;

class Trainer extends Connection
{
    private $name;
    private $department;
    private $course;

    public function set($data){
        // $this->$name       = mysql_real_escape_string($this->con, $data['name']);
        // $this->$department = mysql_real_escape_string($this->con, $data['department']);
        // $this->$course     = mysql_real_escape_string($this->con, $data['course']);
        // $this->$id         = mysql_real_escape_string($this->con, $data['id']);

        /*if(array_key_exists('name', $data)){
            $this->name = $name;
        }
        if(array_key_exists('department', $data)){
            $this->department = $department;
        }
        if(array_key_exists('course', $data)){
            $this->course = $course;
        }
        if(array_key_exists('id', $data)){
            $this->id = $id;
        }
        return $this;/**/
        if(array_key_exists('name', $data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('department', $data)){
            $this->department = $data['department'];
        }
        if(array_key_exists('course', $data)){
            $this->course = $data['course'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
       try{
           $query = "insert into trainer(name, department, course, deleted_at, unique_id) values(:name, :department, :course, :deleted_at, :unique_id)";
           $stmt = $this->con->prepare($query);
           $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
           $stmt->bindValue(':department', $this->department, PDO::PARAM_STR);
           $stmt->bindValue(':course', $this->course, PDO::PARAM_STR);
           $stmt->bindValue(':deleted_at', 1, PDO::PARAM_INT);
           $stmt->bindValue(':unique_id', md5(time()), PDO::PARAM_STR);
           $result = $stmt->execute();
           if($result){
               $_SESSION['insert'] = "Data inserted successfully";
               header("location:index.php");
           }

       }
       catch(PDOException $e){
           echo "Error:".$e->getMessage()."<br>";
           die();
       }
    }

    public function select(){
        try{
            $query  = "select * from trainer where deleted_at = 1 order by id desc";
            $stmt = $this->con->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($result){
                return $result;
            }
        }
        catch(PDOException $e){
            echo "Error:".$e->getMessage()."<br>";
            die();
        }
    }

    public function selectById($id){
      try{
            $query  = "select * from trainer where unique_id = :id ";
            $stmt = $this->con->prepare($query);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if($result){
                return $result;
            }
        }
        catch(PDOException $e){
            echo "Error:".$e->getMessage()."<br>";
            die();
        }
    }

    public function update(){
      $query = "update trainer set 
            name = :name,
            department = :department,
            course = :course
            where unique_id = :id ";

      $stmt = $this->con->prepare($query);
      $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
      $stmt->bindValue(':department', $this->department, PDO::PARAM_STR);
      $stmt->bindValue(':course', $this->course, PDO::PARAM_STR);
      $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
      $stmt->execute();

      if($stmt){
        $_SESSION['update'] = "Data updated successfully";
        header("location:index.php");
      }
    }

    //this mothod does not working...

    public function dataValidate($data = array()){
      if($data['name'] == '' || $data['department'] == '' || $data['course'] == ''){
        $error = "Field must not be empty!";
        header("location:edit.php?error=$error");
      }
      else{
        $name       = mysql_real_escape_string($this->con, $data['name']);
        $department = mysql_real_escape_string($this->con, $data['department']);
        $course     = mysql_real_escape_string($this->con, $data['course']);
        $id         = mysql_real_escape_string($this->con, $data['id']);

        $query = "update trainer set 
              name = :name,
              department = :department,
              course = :course
              where unique_id = :id ";

        $stmt = $this->con->prepare($query);
        $stmt->bindValue(':name', $name, PDO::PARAM_STR);
        $stmt->bindValue(':department', $department, PDO::PARAM_STR);
        $stmt->bindValue(':course', $course, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);
        $stmt->execute();

        if($stmt){
          $_SESSION['update'] = "Data updated successfully";
          header("location:index.php");
        }
      }
    }

     public function delete($id){
        try{
            $query = "delete from trainer where unique_id=:id";
            $stmt = $this->con->prepare($query);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $result = $stmt->execute();
            if($result){
                $_SESSION['delete'] = "Data Deleted Successfully";
                header("location:../trash/trainer.php");
            }
        }
        catch(PDOException $e){
            echo "Error:".$e->getMessage()."<br>";
            die();
        }
    }

    public function tmp_delete($id){
      $query = "update trainer set 
          deleted_at = :deleted_at
          where unique_id = :id ";

      $stmt = $this->con->prepare($query);
      $stmt->bindValue(':deleted_at', 2, PDO::PARAM_INT);
      $stmt->bindValue(':id', $id, PDO::PARAM_STR);
      $result = $stmt->execute();

      if($result){
        $_SESSION['delete'] = "Data Deleted Successfully ";
        header("location:index.php");
      }
    }

    public function selectTrash(){
      $query = "select * from trainer where deleted_at = 2";
      $stmt = $this->con->prepare($query);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($result){
        return $result;
      }
    }

    public function restore($id){
      $query = "update trainer set 
          deleted_at = :deleted_at
          where unique_id = :id ";

      $stmt = $this->con->prepare($query);
      $stmt->bindValue(':deleted_at', 1, PDO::PARAM_INT);
      $stmt->bindValue(':id', $id, PDO::PARAM_STR);
      $result = $stmt->execute();

      if($result){
        $_SESSION['restore'] = "Data restored Successfully ";
        header("location:../trash/trainer.php");
      }
    }
}