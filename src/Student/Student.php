<?php
namespace App\Student;

if(!isset($_SESSION)){
    session_start();
}


use App\Connection;
use FontLib\Table\Type\head;
use PDO;
use PDOException;

class Student extends Connection
{
    private $name;
    private $email;
    private $department;
    private $address;
    private $image;

    /**
     * @param array $data
     * @return $this
     */
    public function set($data = array())
    {
        if(array_key_exists('name', $data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('email', $data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('department', $data)){
            $this->department = $data['department'];
        }
        if(array_key_exists('address', $data)){
            $this->address = $data['address'];
        }
        if(array_key_exists('image', $data)){
            $this->image = $data['image'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    /**
     *
     */
    public function store(){
        $query = "insert into student(name, email, department, address, image, deleted_at,unique_id) values(:name, :email, :department, :address, :image, :deleted_at, :unique_id)";

        $stmt = $this->con->prepare($query);

        $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
        $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
        $stmt->bindValue(':department', $this->department, PDO::PARAM_STR);
        $stmt->bindValue(':address', $this->address, PDO::PARAM_STR);
        $stmt->bindValue(':image', $this->image, PDO::PARAM_STR);
        $stmt->bindValue(':deleted_at', 1, PDO::PARAM_INT);
        $stmt->bindValue(':unique_id', md5(time()), PDO::PARAM_STR);
        $result = $stmt->execute();

        if($result){
            $_SESSION['insert'] = "Data inserted successfully";
            header('Location:index.php');
        }
    }

    public function select(){
        try{
            $query = "select * from student WHERE deleted_at=1 order by id desc";
            $stmt = $this->con->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($result){
                return $result;
            }
        }
        catch(PDOException $e){
            echo "Error:".$e->getMessage()."<br>";
            die();
        }
    }

    public function view($id){
        try{
            $query = "select * from student where unique_id = :id ";
            $stmt = $this->con->prepare($query);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if($result){
                return $result;
            }
        }
        catch(PDOException $e){
            echo "Error:".$e->getMessage()."<br>";
            die();
        }
    }

    public function edit($id){
        try{
            $query = select ;
        }
        catch(PDOException $e){
            echo "Error:".$e->getMessage()."<br>";
            die();
        }
    }

    public function delete($id){
        try{
            $query = "delete from student where unique_id=:id";
            $stmt = $this->con->prepare($query);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $result = $stmt->execute();
            if($result){
                $_SESSION['delete'] = "Data Deleted Successfully";
                header("location:../trash/student.php");
            }
        }
        catch(PDOException $e){
            echo "Error:".$e->getMessage()."<br>";
            die();
        }
    }

    public function tmp_delete($id){
        try{
            $query = "update student set
                  deleted_at = :upd
                  WHERE  unique_id = :id ";
            $stmt = $this->con->prepare($query);
            $stmt->bindValue(':upd', 2, PDO::PARAM_INT);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $result = $stmt->execute();
            if($result){
                $_SESSION['delete'] = "Data Deleted Successfully";
                header("location:index.php");
            }

        }

        catch(PDOException $e){
            echo "Error:".$e->getMessage()."<br>";
            die();
        }
    }

    public function selectTrash(){
        try{
            $query = "select * from student where deleted_at=2";
            $stmt = $this->con->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($result){
                //$_SESSION['restore'] = "Data Restored Successfully";
                //header('location:index.php');
                return $result;
            }
        }
        catch(PDOException $e){
            echo "Error:".$e->getMessage()."<br>";
            die();
        }
    }

    public function restore($id){
        try{
           $query = "update student set
                  deleted_at = :restore
                  where unique_id = :id ";
            $stmt = $this->con->prepare($query);
            $stmt->bindValue(':restore', 1,PDO::PARAM_INT);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $result = $stmt->execute();
            if($result){
                $_SESSION['restore'] = "Data Restored Successfully";
                header("location:../trash/student.php");
            }
        }
        catch(PDOException $e){
            echo "Error:".$e->getMessage()."<br>";
            die();
        }
    }

    public function update(){
        $query = "update student set 
                name = :name,
                email = :email,
                department = :department,
                address = :address,
                image = :image
                where unique_id = :id  ";
        $stmt = $this->con->prepare($query);

        $stmt->bindValue(':name',$this->name, PDO::PARAM_STR);
        $stmt->bindValue(':email',$this->email, PDO::PARAM_STR);
        $stmt->bindValue(':department',$this->department, PDO::PARAM_STR);
        $stmt->bindValue(':address',$this->address, PDO::PARAM_STR);
        $stmt->bindValue(':image',$this->image, PDO::PARAM_STR);
        $stmt->bindValue(':id',$this->id, PDO::PARAM_STR);
        $result = $stmt->execute();

        if($result){
            $_SESSION['update'] = "Data Updated Successfully";
            header("location:index.php");
        }
    }
}