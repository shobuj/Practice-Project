<?php

namespace App\Helper;

use App\Connection;
use PDO;

class Helper extends Connection
{

	public function image_delete($id){
		$stmt = $this->con->prepare('select image from student where unique_id = :id ');
		$stmt->bindValue(':id', $id, PDO::PARAM_STR);
		$stmt->execute();
		$image_name = $stmt->fetch(PDO::FETCH_ASSOC);
		$result = "../uploads/".$image_name['image'];

		if(isset($result)){
			unlink($result);
		}
	}

    public function image_uploaded(){
        $img_name = $_FILES['image']['name'];
        $tmp_name = $_FILES['image']['tmp_name'];
        $ext_name = end(explode('.', $img_name));
        $name     = substr(md5(time()),'0', '8');
        $_POST['image'] = $name.'.'.$ext_name;

        move_uploaded_file($tmp_name, '../uploads/'.$_POST['image']);
        return $_POST['image'];
    }
}