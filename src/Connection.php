<?php

namespace App;

use PDO;
use PDOException;


class Connection
{
    protected $con;
    private $user = "root";
    private $pass = "";

    /**
     * Connection constructor.
     */
    public function __construct()
    {
       try{
           $this->con = new PDO("mysql:host=localhost;dbname=practiceProject", $this->user, $this->pass);
           $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       }
       catch(PDOException $e){
           echo "Error: ".$e->getMessage()."<br>";
           die();
       }

    }
}