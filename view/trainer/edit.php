<?php
include_once '../includes/header.php';
include_once '../../vendor/autoload.php';

$trainer = new App\Trainer\Trainer();

$data = $trainer->selectById($_GET['edit']);


?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12" style="text-align: center">
                <h1 class="page-header" style="color: green">Add Trainer</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">

                <?php if(isset($_GET['error'])){
                   $error = $_GET['error'];
                    echo '<div class = "alert alert-danger" style="text-align:center;">'.$error.' </div>';
                } ?>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-2">
                                <form role="form" action="view/trainer/update.php" method="POST">
                                    <div class="form-group">
                                        <label>Trainer Name</label>
                                        <input name="name" type="text" value="<?= $data['name']; ?>" class="form-control">
                                        <input name="id" type="hidden" value="<?= $data['unique_id']; ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Department</label>
                                        <select name="department" id="" class="form-control">
                                            <option>Select One</option>
                                            <option  <?=  ($data['department'] == 'CSE')?'selected':'' ?> value="CSE">CSE</option>
                                            <option <?= ($data['department'] == 'BBA')?'selected':'' ?> value="BBA">BBA</option>
                                            <option <?= ($data['department'] == 'EEE')?'selected':'' ?> value="EEE">EEE</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Course</label>
                                        <select name="course" id="" class="form-control">
                                            <option>Select One</option>
                                            <option <?= ($data['course'] == 'JAVA')?'selected':'' ?> value="JAVA">JAVA</option>
                                            <option <?= ($data['course'] == 'PHP')?'selected':'' ?> value="PHP">PHP</option>
                                            <option <?= ($data['course'] == 'GRAPHICS')?'selected':'' ?> value="GRAPHICS">GRAPHICS</option>
                                        </select>
                                    </div>
                                   <!--  <div class="form-group">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" name="course[]" value="java" class="custom-control-input">
                                            <span class="custom-control-description">Java</span>
                                        </label>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" name="course[]" value="php" class="custom-control-input">
                                            <span class="custom-control-description">PHP</span>
                                        </label>
                                    </div> -->

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn btn-default">Reset Button</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->

                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../includes/footer.php';
?>