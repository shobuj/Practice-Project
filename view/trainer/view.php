<?php
if(!isset($_SESSION)){
    session_start();
}
include_once '../includes/header.php';
include_once '../../vendor/autoload.php';

$trainer = new App\Trainer\Trainer();
$data = $trainer->selectById($_GET['view']);
$i=1;
/*echo "<pre>";
var_dump($data);*/

?>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12" style="text-align: center">
                    <h1 class="page-header" style="color: green">Trainer Profile</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
          
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead >
                                <tr class="text-center" >
                                    <th class="text-center">Serial</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Course</th>
                                </tr>
                                </thead>
                                <tbody>
                            
                                <tr class="odd gradeX" style="text-align: center">
                                    <td><?= $i; ?></td>
                                    <td><?= $data['name']; ?></td>
                                    <td><?= $data['department']; ?></td>
                                    <td><?= $data['course']; ?></td>
                                    
                                </tr>
                               
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
<?php
include_once '../includes/footer.php';
?>