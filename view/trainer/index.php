<?php
if(!isset($_SESSION)){
    session_start();
}
include_once '../includes/header.php';
include_once '../../vendor/autoload.php';

$trainer = new App\Trainer\Trainer();
$data = $trainer->select();
/*echo "<pre>";
var_dump($data);*/

?>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12" style="text-align: center">
                    <h1 class="page-header" style="color: green">All Trainer</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           <div style="position: fixed; z-index: 111; right: 50px">
               <?php
               if(isset($_SESSION['insert'])){
                   echo "<div class='alert alert-success'>".$_SESSION['insert']."</div>";
                   session_unset();
               }
               if(isset($_SESSION['update'])){
                   echo "<div class='alert alert-success'>".$_SESSION['update']."</div>";
                   session_unset();
               }
               if(isset($_SESSION['delete'])){
                   echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                   session_unset();
               }
               ?>
           </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead >
                                <tr class="text-center" >
                                    <th class="text-center">Serial</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Course</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                            <?php
                            $i = 0;
                            foreach($data as $result){
                                $i++;

                                ?>
                                <tr class="odd gradeX" style="text-align: center">
                                    <td><?= $i; ?></td>
                                    <td><?= $result['name']; ?></td>
                                    <td><?= $result['department']; ?></td>
                                    <td><?= $result['course']; ?></td>
                                    <td>
                                        <a  class="btn btn-success" href="view/trainer/view.php?view=<?= $result['unique_id']; ?>">View</a>
                                        <a class="btn btn-primary" href="view/trainer/edit.php?edit=<?= $result['unique_id']; ?>">Edit</a>
                                        <a data-toggle="modal" data-target="#myModal"  class="btn btn-danger delete" data-id="<?= $result['unique_id'] ?>" href="javascript:void(0)">Delete</a>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>

                       <div id="myModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="view/trainer/tmp_delete.php" method="get">
                                    <input type="hidden" name="delete" id="delete">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure to delete!</h4>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>

                            </div>
                        </div>

                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
<?php
include_once '../includes/footer.php';
?>