<?php
include_once '../includes/header.php';
?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12" style="text-align: center">
                <h1 class="page-header" style="color: green">Add Trainer</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-2">
                                <form role="form" action="view/trainer/store.php" method="POST">
                                    <div class="form-group">
                                        <label>Trainer Name</label>
                                        <input name="name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Department</label>
                                        <select name="department" id="" class="form-control">
                                            <option>Select One</option>
                                            <option value="CSE">CSE</option>
                                            <option value="BBA">BBA</option>
                                            <option value="EEE">EEE</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Course</label>
                                        <select name="course" id="" class="form-control">
                                            <option>Select One</option>
                                            <option value="JAVA">JAVA</option>
                                            <option value="PHP">PHP</option>
                                            <option value="GRAPHICS">GRAPHICS</option>
                                        </select>
                                    </div>
                                   <!--  <div class="form-group">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" name="course[]" value="java" class="custom-control-input">
                                            <span class="custom-control-description">Java</span>
                                        </label>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" name="course[]" value="php" class="custom-control-input">
                                            <span class="custom-control-description">PHP</span>
                                        </label>
                                    </div> -->

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn btn-default">Reset Button</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->

                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../includes/footer.php';
?>