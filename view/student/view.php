<?php
if(!isset($_SESSION)){
    session_start();
}
include_once '../includes/header.php';
include_once '../../vendor/autoload.php';

$student = new App\Student\Student();
$result  = $student->view($_GET['view']);


?>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">All Students</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div style="position: fixed; z-index: 111; right: 50px">
                <?php
                if(isset($_SESSION['insert'])){
                    echo "<div class='alert alert-success'>".$_SESSION['insert']."</div>";
                    session_unset();
                }
                ?>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead >
                                <tr class="text-center" >

                                    <th class="text-center" >Name</th>
                                    <th class="text-center" >Email</th>
                                    <th class="text-center" >Department</th>
                                    <th class="text-center" >Adress</th>
                                    <th class="text-center" >Image</th>

                                </tr>
                                </thead>
                                <tbody>

                                    <tr class="odd gradeX" style="text-align: center">

                                        <td><?= $result['name']; ?></td>
                                        <td><?= $result['email']; ?></td>
                                        <td><?= $result['department']; ?></td>
                                        <td><?= $result['address']; ?></td>
                                        <td>
                                            <img width="160px" src="view/uploads/<?= $result['image']; ?>" alt="no image">
                                        </td>

                                    </tr>

                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->




                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
<?php
include_once '../includes/footer.php';
?>