<?php
include_once '../includes/header.php';
include_once '../../vendor/autoload.php';
$student = new App\Student\Student();
$result = $student->view($_GET['edit']);
?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12" style="text-align: center;">
                <h1 class="page-header" style="color: green;">Update Student</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <!--                <div class="panel panel-default">-->

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-8 col-md-offset-2">
                            <form role="form" action="view/student/update.php" method="POST" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Student Name</label>
                                    <input type="text" name="name" value="<?= $result['name']; ?>" class="form-control">
                                    <input type="hidden" name="id" value="<?= $result['unique_id']; ?>" class="form-control">
                                    <input type="hidden" name="image" value="<?= $result['image']; ?>" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" name="email" value="<?= $result['email']; ?>" class="form-control" id="">
                                </div>
                                <div class="form-group">
                                    <label>Department</label>
                                    <select name="department" id="" class="form-control">
                                        <option>Select One</option>
                                        <option <?= ($result['department'] == 'CSE')?'selected':''  ?> value="CSE">CSE</option>
                                        <option <?= ($result['department'] == 'BBA')?'selected':'' ?> value="BBA">BBA</option>
                                        <option <?= ($result['department'] == 'EEE')?'selected':'' ?> value="EEE">EEE</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea type="text" name="address" class="form-control" rows="3"><?= $result['address']; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <div>
                                        <img width="120px" src="view/uploads/<?= $result['image']; ?>" alt="no image">

                                    </div>
                                    <label for="">Image</label>
                                    <input type="file" name="image" id="">
                                </div>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
                <!--                </div>-->
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../includes/footer.php';
?>